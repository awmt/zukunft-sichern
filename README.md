#Beim Frontend muss die .env aus der .env.example gebaut werden mit der gewünschten Konfiguration.
#API_AUTH muss hier der gleiche code sein wie auch im .env File der API.
#Mit dem Befehl `npm run docker:build` wird das Docker image des frontends gebaut.


#AWMT Zukunft-sichern

#Blog Background Image from Pexels:
#Link: https://www.pexels.com/photo/green-pine-trees-1179229/ | Author: Brandon Montrone | Title: Green Pine Trees | License: free to use(Pexels License)

# Prismic Nuxt.js Example Blog

> [Nuxt.js](https://nuxtjs.org) example blog project with content managed in [Prismic](https://prismic.io)

## Check out our article for a step by step guide to getting this project up and running

> [Prismic project guide](https://user-guides.prismic.io/examples/nuxt-js-samples/create-a-sample-blog-with-prismic-and-nuxt)

## Launch this project in your local environment

Run the following commands:

```bash
npm install
npm run dev
```

Then you can access it at [http://localhost:3000](http://localhost:3000).

## License

This software is licensed under the Apache 2 license, quoted below.

Copyright 2013-2019 Prismic (http://prismic.io).

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this project except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
