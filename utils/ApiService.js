const fetch = require("node-fetch");

class ApiService {
  url;
  authorization;
  headers;

  constructor(props) {
    const { url, code } = props;

    this.url = url;
    this.authorization = code;
    this.headers = {
      "Content-Type": "application/json",
      Authorization: this.authorization,
    };
  }

  async getNews() {
    const endpoint = "api/news";

    return await this.request({ method: "GET", endpoint });
  }

  async request(params) {
    const { method, body, endpoint } = params;

    const res = await fetch(this.url + endpoint, { method, body, headers: this.headers });

    return await res.json();
  }

  async get(endpoint) {
    return await this.request({ method: "GET", endpoint });
  }

  async post(data, endpoint) {
    return await this.request({ method: "POST", body: JSON.stringify(data), endpoint });
  }

  async put(data, endpoint) {
    return await this.request({ method: "PUT", body: JSON.stringify(data), endpoint });
  }
}

export default ApiService;
