const pkg = require("./package");

module.exports = {
  target: "server",
  mode: "universal",

  buildModules: [
    '@nuxtjs/moment',
    '@aceforth/nuxt-optimized-images',

  ],
  optimizedImages: {
    inlineImageLimit: -1,
    handleImages: ['jpeg', 'png', 'svg', 'webp', 'gif'],
    optimizeImages: true,
    optimizeImagesInDev: true,
    defaultImageLoader: 'img-loader',
    mozjpeg: {
      quality: 65
    },
    webp: {
      quality: 65
    }
  },
    optipng: false,
    pngquant: {
      speed: 7,
      quality: [0.55, 0.7]
  },
  moment: {
    defaultLocale: 'de',
    locales: ['de']
  },

  optimization: {
    minimize: true
  },
  /*
   ** Headers of the page
   */
  head: {
    title: "AWMT - Zukunft Sichern",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "Prismic + Nuxt Blog example",
      },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel:"preload",
        href: "https://fonts.googleapis.com/css?family=Lato:300,400,700,900",
      },
      {
        rel:"preload",
        href: "https://fonts.googleapis.com/css2?family=Dosis:wght@500&display=swap",
      },
      {
        rel: "preload",
        href: "https://fonts.googleapis.com/css2?family=Archivo+Black&display=swap",
      },


    ],
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },

  /*
   ** Global CSS
   */
  css: ["@/assets/css/resetr.css", "@/assets/css/common.scss"],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: ["~plugins/gtm.js","~plugins/vuetify.js", "plugins/vue-social-sharing.js", { src: "~/plugins/prismicLinks", ssr: false }],
  /*
   ** Nuxt.js modules
   */
  modules: [
    [
      "@nuxtjs/prismic",
      {
        endpoint: "https://awmt.cdn.prismic.io/api/v2",
      },
    ],
    ["nuxt-sm"],
    "nuxt-clipboard2",
    ["@nuxtjs/dotenv", {}],
  ],
  vuetify: {
    theme: {
      treeShake: true,
      options: {
        customProperties: true,
      },
      dark: false,
      light: false,
      themes: {
        light: {
        },
      },
    },
  },
  prismic: {
    endpoint: "https://awmt.cdn.prismic.io/api/v2",
    linkResolver: "@/plugins/link-resolver",
    htmlSerializer: "@/plugins/html-serializer",
    preview: false,
  },

  loaders: [
    {
      test: /\.(jpe?g|png)$/i,
      loaders: [
        'file-loader',
        'webp-loader'
      ]
    }
  ],

  /*
   ** Build configuration
   */
  build: {
      transpile: ['countup.js', 'vue-countup-v2'],
    vendor: ['vue-social-sharing'],
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
    buildDir: "./server",
  },

  generate: {
    fallback: "404.html", // Netlify reads a 404.html, Nuxt will load as an SPA
  },
};
