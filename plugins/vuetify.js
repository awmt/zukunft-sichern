import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "@mdi/font/css/materialdesignicons.css"; // Ensure you are using css-loader version "^2.1.1" ,Vue.use(Vuetify)

Vue.use(Vuetify);
//toDO Farben gehen nicht
export default ctx => {
  const vuetify = new Vuetify({
    theme: {
      treeShake: true,
      options: {
        customProperties: true,
      },
      dark: false,
      themes: {
        light: {
          anchor:"#186368",
          primaryColor: "#186368",
          twitterColor: "#1da1f2",
          componentGrey: "#454745",
          textGrey:"#545454",
        },
      },
    },
  });

  ctx.app.vuetify = vuetify;
  ctx.$vuetify = vuetify.framework;
};
