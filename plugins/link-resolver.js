/**
 * To learn more about Link Resolving check out the Prismic documentation
 */

export default function (doc) {
  if (doc.isBroken) {
    return "/not-found";
  }

  if (doc.type === "blog_home") {
    return "/Blog";
  }

  if (doc.type === "post") {
    return "/blog/" + doc.uid;
  }
  if (doc.type === "blogentries") {
    return "/Blog/";
  }

  return "/not-found";
}
