export const state = () => ({
  dataProtect: {},
  shareTweet: {},
  tweetInfo: {},
})

export const mutations = {
  setDataprotect(state, dataProtect) {
    state.dataProtect = dataProtect
  },
  setShareTweet(state, shareTweet) {
    state.shareTweet = shareTweet
  },
  setTweetInfo(state, tweetInfo) {
    state.tweetInfo = tweetInfo
  }
}

export const actions = {
  async loadDataProtect({ commit }) {
    const dataProtect = (await this.$prismic.api.getSingle('dataprotection')).data;
    commit('setDataprotect', dataProtect)
  },
  async loadShareTweet({ commit }) {
    const shareTweet = (await this.$prismic.api.getSingle('sharetweet')).data;
    commit('setShareTweet', shareTweet)
  },
  async loadTweetInfo({ commit }) {
    const tweetInfo = (await this.$prismic.api.getSingle('tweetinfo')).data;
    commit('setTweetInfo', tweetInfo)
  }
}
